import { randomUUID } from 'node:crypto'

import { faker } from '@faker-js/faker'
import { PrismaClient } from '@prisma/client'

import { SocialType } from '../src/utils/enums'

const repeatAsync = <T>(times: number, fn: (index: number) => T) => {
  const result: T[] = []

  for (const index of [...Array(times).keys()]) {
    result.push(fn(index))
  }

  return Promise.all(result)
}

const curricumId = randomUUID()

async function prismaSeed() {
  const prisma = new PrismaClient()

  const deleteAll = async () => {
    await prisma.academicExperience.deleteMany()
    await prisma.course.deleteMany()
    await prisma.professionalExperience.deleteMany()
    await prisma.curriculum.deleteMany()
    await prisma.socialMedia.deleteMany()
    await prisma.user.deleteMany()
  }

  await deleteAll()

  try {
    const user = await prisma.user.create({
      data: {
        email: faker.internet.email(),
        name: faker.person.firstName(),
        lastname: faker.person.lastName(),
        password: '$2a$10$W4z2zpZyNY63BvpFkazbMOlxC1VfVLfgnXa4hxYNC.9up23bgRjaq' /* 12345678 */,

        curriculum: {
          create: {
            id: curricumId
          }
        }

      }
    })

    const socialMedias = [
      SocialType.EMAIL,
      SocialType.GIT_HUB,
      SocialType.GIT_LAB,
      SocialType.LINKEDIN,
    ].map((socialMedia) => ({
      reference: faker.internet.email(),
      socialType: socialMedia,
      userId: user.id
    }))

    await prisma.socialMedia.createMany({ data: socialMedias })

    repeatAsync(2, async () => {
      await prisma.academicExperience.create({
        data: {
          name: faker.company.name(),
          startDate: faker.date.between({
            from: '2023-01-01T00:00:00.000Z',
            to: '2023-12-01T00:00:00.000Z'
          }),
          endDate: faker.date.between({
            from: '2024-01-01T00:00:00.000Z',
            to: new Date()
          }),
          description: faker.lorem.paragraph(),
          location: faker.location.state(),
          position: faker.person.jobArea(),

          curriculumId: curricumId
        }
      })

      await prisma.professionalExperience.create({
        data: {
          name: faker.company.name(),
          startDate: faker.date.between({
            from: '2023-01-01T00:00:00.000Z',
            to: '2023-12-01T00:00:00.000Z'
          }),
          endDate: faker.date.between({
            from: '2024-01-01T00:00:00.000Z',
            to: new Date()
          }),
          description: faker.lorem.paragraph(),
          location: faker.location.state(),
          role: faker.person.jobTitle(),

          curriculumId: curricumId
        }
      })

      await prisma.course.create({
        data: {
          name: faker.person.jobArea() + ' ' + faker.person.jobType(),
          time: faker.string.numeric(3),
          description: faker.lorem.paragraph(),
          company: faker.company.name(),
          status: 'DONE',

          curriculumId: curricumId
        }
      })
    })

  } catch (e) {
    await deleteAll()
    throw e
  }

  await prisma.$disconnect()

  console.log("Seed's done! 🌱")
}

prismaSeed()