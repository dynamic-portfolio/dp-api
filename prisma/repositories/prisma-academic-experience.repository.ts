import { PrismaService } from "prisma/client"

import { AcademicExperienceRepository } from "@/repositories/academic-experience.repository"
import { AcademicExperience } from "@/types/academic-experience"
import { SetOptional, SetRequired } from "type-fest"

export class PrismaAcademicExperienceRepository extends AcademicExperienceRepository {
  constructor(private readonly prisma: PrismaService) {
    super()
  }

  async register(academicExperience: SetOptional<AcademicExperience, "id">) {
    return await this.prisma.academicExperience.create({ data: academicExperience })
  }

  async update(academicExperience: SetRequired<Partial<AcademicExperience>, 'id'>) {
    return await this.prisma.academicExperience.update({
      where: {
        id: academicExperience.id,
      },
      data: {
        ...academicExperience
      }
    })
  }

  async delete(id: string) {
    await this.prisma.academicExperience.delete({
      where: {
        id
      }
    })
  }
}