import { PrismaService } from "prisma/client"

import { SocialMediaRepository } from "@/repositories/social-media.repository"
import { AsyncMaybe, SocialMedia } from "@/types"
import { SocialType } from "@/utils/enums"

export class PrismaSocialMediaRepository extends SocialMediaRepository {
  constructor(private readonly prisma: PrismaService) {
    super()
  }

  async findByStudentIdAndSocialType(userId: string, socialType: SocialType): AsyncMaybe<SocialMedia> {
    const socialMedia = await this.prisma.socialMedia.findFirst({
      where: {
        userId,
        socialType: socialType.toString()
      }
    })

    if (!socialMedia) return null
    return socialMedia
  }

  async register(socialMedia: SocialMedia): Promise<SocialMedia> {
    return await this.prisma.socialMedia.create({ data: socialMedia })
  }

  async update(socialMedia: SocialMedia): Promise<SocialMedia> {
    return await this.prisma.socialMedia.update({
      where: {
        id: socialMedia.id,
      },
      data: {
        ...socialMedia
      }
    })
  }

  async delete(id: string): Promise<void> {
    await this.prisma.socialMedia.delete({
      where: {
        id
      }
    })
  }


}