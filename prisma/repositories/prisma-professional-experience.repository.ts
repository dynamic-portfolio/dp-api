import { PrismaService } from "prisma/client"

import { ProfessionalExperienceRepository } from "@/repositories/professional-experience.repository"
import { ProfessionalExperience } from "@/types"
import { SetOptional, SetRequired } from "type-fest"

export class PrismaProfessionalExperienceRepository extends ProfessionalExperienceRepository {
  constructor(private readonly prisma: PrismaService) {
    super()
  }

  async register(professionalExperience: SetOptional<ProfessionalExperience, "id">) {
    return await this.prisma.professionalExperience.create({ data: professionalExperience })
  }

  async update(professionalExperience: SetRequired<Partial<ProfessionalExperience>, 'id'>) {
    return await this.prisma.professionalExperience.update({
      where: {
        id: professionalExperience.id,
      },
      data: {
        ...professionalExperience
      }
    })
  }

  async delete(id: string) {
    await this.prisma.professionalExperience.delete({
      where: {
        id
      }
    })
  }
}