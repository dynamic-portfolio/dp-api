import { PrismaService } from "prisma/client"

import { UserRepository } from "@/repositories/user.repository"
import { AsyncMaybe, SocialMedia, User } from "@/types"

export class PrismaUserRepository extends UserRepository {
    constructor(private readonly prisma: PrismaService) {
        super()
    }

    async create(user: Omit<User, 'id' | 'curriculum' | 'socialMedia'>) {
        return await this.prisma.user.create({ data: user })
    }

    async findUserById(userId: string): AsyncMaybe<User & { socialMedia: SocialMedia[] }> {
        const user = await this.prisma.user.findFirst({
            where: {
                id: userId
            },
            include: {
                socialMedia: true
            }
        })

        if (!user) return null;

        return user
    }
}
