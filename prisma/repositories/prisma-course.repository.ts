import { PrismaService } from "prisma/client"

import { CourseRepository } from "@/repositories/course.repository"
import { Course } from "@/types"
import { SetOptional, SetRequired } from "type-fest"

export class PrismaCourseRepository extends CourseRepository {
  constructor(private readonly prisma: PrismaService) {
    super()
  }

  async register(course: SetOptional<Course, "id">) {
    return await this.prisma.course.create({ data: course })
  }

  async update(course: SetRequired<Partial<Course>, 'id'>) {
    return await this.prisma.course.update({
      where: {
        id: course.id,
      },
      data: {
        ...course
      }
    })
  }

  async delete(id: string) {
    await this.prisma.course.delete({
      where: {
        id
      }
    })
  }
}