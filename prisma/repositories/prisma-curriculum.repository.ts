import { PrismaService } from "prisma/client"

import { CurricumRepository } from "@/repositories/curriculum.repository"
import { AcademicExperience, AsyncMaybe, Curriculum, User } from "@/types"

export class PrismaCurriculumRepository extends CurricumRepository {

  constructor(private readonly prisma: PrismaService) {
    super()
  }

  async create(userId: string): Promise<void> {
    await this.prisma.curriculum.create({
      data: {
        userId,
      },
    })
  }

  async findCurriculumByUserId(userId: string): AsyncMaybe<Curriculum> {
    const curriculum = await this.prisma.curriculum.findFirst({
      where: {
        userId
      },
      include: {
        user: {
          include: {
            socialMedia: true
          }
        },
        academicExperiences: true,
        courses: true,
        professionalExperiences: true
      }
    })

    if (!curriculum) return null

    return curriculum
  }
}
