/*
  Warnings:

  - You are about to drop the column `userId` on the `academic_experiences` table. All the data in the column will be lost.
  - Added the required column `curriculumId` to the `academic_experiences` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "academic_experiences" DROP CONSTRAINT "academic_experiences_userId_fkey";

-- AlterTable
ALTER TABLE "academic_experiences" DROP COLUMN "userId",
ADD COLUMN     "curriculumId" TEXT NOT NULL;

-- CreateTable
CREATE TABLE "resumes" (
    "id" TEXT NOT NULL,
    "userId" TEXT NOT NULL,

    CONSTRAINT "resumes_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "resumes_userId_key" ON "resumes"("userId");

-- AddForeignKey
ALTER TABLE "resumes" ADD CONSTRAINT "resumes_userId_fkey" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "academic_experiences" ADD CONSTRAINT "academic_experiences_curriculumId_fkey" FOREIGN KEY ("curriculumId") REFERENCES "resumes"("id") ON DELETE CASCADE ON UPDATE CASCADE;
