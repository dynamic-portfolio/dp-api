import { FastifyInstance } from 'fastify';

import { userRoutes } from '@/user';
import { UserCases } from '@/user/services'

import { prisma } from 'prisma/client'
import { PrismaUserRepository } from 'prisma/repositories/prisma-user.repository'
import { PrismaAcademicExperienceRepository } from 'prisma/repositories/prisma-academic-experience.repository'
import { PrismaProfessionalExperienceRepository } from 'prisma/repositories/prisma-professional-experience.repository'
import {
    AcademicExperienceCases,
    CourseCases,
    CurriculumCases,
    ProfessionalExperienceCases
} from '@/curriculum/services';

import { curriculumRoutes } from '@/curriculum/routes';
import { PrismaCourseRepository } from 'prisma/repositories/prisma-course.repository';
import { SocialMediaCases } from '@/user/services/social-media-cases';
import { PrismaSocialMediaRepository } from 'prisma/repositories/prisma-social-media.repository';
import { PrismaCurriculumRepository } from 'prisma/repositories/prisma-curriculum.repository';

export default async function routes(fastify: FastifyInstance): Promise<void> {
    //Repositories
    const userRepository = new PrismaUserRepository(prisma)
    const socialMediaRepository = new PrismaSocialMediaRepository(prisma)
    const academicExperience = new PrismaAcademicExperienceRepository(prisma)
    const course = new PrismaCourseRepository(prisma)
    const curriculumRepository = new PrismaCurriculumRepository(prisma)
    const professionalExperience = new PrismaProfessionalExperienceRepository(prisma)

    //Services
    const useCases = new UserCases(userRepository, curriculumRepository)
    const socialMediaCases = new SocialMediaCases(socialMediaRepository)
    const academicExperienceCases = new AcademicExperienceCases(academicExperience)
    const courseCases = new CourseCases(course)
    const professionalExperienceCases = new ProfessionalExperienceCases(professionalExperience)
    const curriculumCases = new CurriculumCases(curriculumRepository)

    //Routes
    userRoutes(fastify, useCases, socialMediaCases);
    curriculumRoutes(
        fastify,
        academicExperienceCases,
        professionalExperienceCases,
        courseCases,
        curriculumCases
    )
}
