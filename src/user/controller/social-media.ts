import { FastifyReply, FastifyRequest } from "fastify"

import { SocialMediaCases } from "../services/social-media-cases"
import { EnsureSocialType, SocialMedia } from "@/types"



export class SocialMediaController {
  private readonly socialMediaCases: SocialMediaCases

  constructor(socialMediaCases: SocialMediaCases) {
    this.socialMediaCases = socialMediaCases
  }

  async updateSocialMediaHandler(request: FastifyRequest, reply: FastifyReply) {
    const socialMediaData: Omit<SocialMedia, 'id'> & EnsureSocialType
      = request.body as Omit<SocialMedia, 'id'> & EnsureSocialType

    return reply.code(200).send((await this.socialMediaCases.update(socialMediaData)).value)
  }

  async deleteAcademicExperienceHandler(request: FastifyRequest, reply: FastifyReply) {
    const socialMediaData: { id: string } = request.body as { id: string }

    return reply.code(200).send((await this.socialMediaCases.delete(socialMediaData)).value)
  }
}
