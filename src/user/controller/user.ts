import { FastifyReply, FastifyRequest } from "fastify"

import { UserCases } from "@/user/services";
import { User } from "@prisma/client";

export class UserController {
  private readonly userCases: UserCases

  constructor(userCases: UserCases) {
    this.userCases = userCases
  }

  async createUserHandler(request: FastifyRequest, reply: FastifyReply) {
    const userData: Omit<User, 'id'> = request.body as Omit<User, 'id'>

    return reply.code(200).send((await this.userCases.create(userData)).value)
  }

  async getUserHandler(request: FastifyRequest, reply: FastifyReply) {
    const { userId } = request.query as { userId: string }

    return reply.code(200).send((await this.userCases.getUser({ userId } as { userId: string })).value)
  }
}