import { describe, expect, it, vi } from 'vitest'

import { asMock } from '@/utils/asMock'
import { SocialMediaCases } from './social-media-cases'
import { SocialMediaRepository } from '@/repositories/social-media.repository'
import { SocialType } from '@/utils/enums'

const socialMedia = {
  reference: 'linkedin_media',
  socialType: SocialType.LINKEDIN,
  userId: 'user_id'
}

const updateSocialMedia = { ...socialMedia, reference: 'github_media', socialType: SocialType.GIT_HUB }
const deleteSocialMedia = { ...socialMedia, id: 'social_media_id' }

const socialMediaRepositoryMock = {
  register: vi.fn().mockResolvedValue(socialMedia),
  update: vi.fn().mockResolvedValue(socialMedia),
  delete: vi.fn().mockResolvedValue(deleteSocialMedia.id),
  findByStudentIdAndSocialType: vi
    .fn()
    .mockResolvedValue({
      userId: socialMedia.userId, socialType: socialMedia.socialType
    })
}

const socialMediaUseCase =
  new SocialMediaCases(
    asMock<SocialMediaRepository>(socialMediaRepositoryMock)
  )

describe('Social media use cases', () => {
  it('should register social media sucessfuly with all required fields', async () => {
    socialMediaRepositoryMock.findByStudentIdAndSocialType.mockReturnValue(null)

    const result = await socialMediaUseCase.update(socialMedia)

    expect(result.isRight()).toBe(true)
    expect(result.value).toEqual({
      message: 'Social media updated successfully',
      handle: { socialMedia: socialMedia }
    })
    expect(socialMediaRepositoryMock.findByStudentIdAndSocialType).toHaveBeenCalledWith(
      socialMedia.userId, socialMedia.socialType
    )
    expect(socialMediaRepositoryMock.register).toHaveBeenCalledWith(socialMedia)
    expect(socialMediaRepositoryMock.update).toReturnTimes(0)
  })

  it('should update social media sucessfuly with all required fields', async () => {
    socialMediaRepositoryMock.findByStudentIdAndSocialType.mockReturnValue(updateSocialMedia)

    const result = await socialMediaUseCase.update(socialMedia)

    expect(result.isRight()).toBe(true)
    expect(result.value).toEqual({
      message: 'Social media updated successfully',
      handle: { socialMedia: socialMedia }
    })
    expect(socialMediaRepositoryMock.findByStudentIdAndSocialType).toHaveBeenCalledWith(
      socialMedia.userId, socialMedia.socialType
    )
    expect(socialMediaRepositoryMock.update).toHaveBeenCalledWith(socialMedia)
  })

  it('should delete professional experience successfully', async () => {
    const result = await socialMediaUseCase.delete({ id: deleteSocialMedia.id })

    expect(result.isRight()).toBe(true)
    expect(result.value).toEqual({
      message: "Social media deleted successfully",
      handle: {}
    })
    expect(socialMediaRepositoryMock.delete).toHaveBeenCalledWith(deleteSocialMedia.id)
  })
})
