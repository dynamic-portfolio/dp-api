import { z } from 'zod'

import { DomainError } from '@/errors/domain-error'
import { IServiceResponse } from '@/types/service-response'
import { AsyncEither, left, right } from '@/utils/either'
import { SocialMediaRepository } from '@/repositories/social-media.repository'
import {
  SocialMedia,
  deleteSocialMediaSchema,
  updateSocialMediaSchema
} from '@/types'
import { ValidationError } from '@/errors/validation-error'
import { GenericError } from '@/errors/generic-error'

type UpdateSocialMediaDto = z.infer<typeof updateSocialMediaSchema>
type DeleteSocialMediaDto = z.infer<typeof deleteSocialMediaSchema>
type SocialMediaResponse = AsyncEither<DomainError, IServiceResponse>

export class SocialMediaCases {
  constructor(
    private readonly socialMediaRepository: SocialMediaRepository
  ) { }

  async update(updateSocialMediaDto: UpdateSocialMediaDto): SocialMediaResponse {
    const payload = updateSocialMediaSchema.safeParse(updateSocialMediaDto)

    if (!payload.success) return left(new ValidationError(payload.error))

    const { data } = payload

    const newData = {
      reference: data.reference,
      socialType: data.socialType,
      userId: data.userId
    }

    const socialMedia = await this.socialMediaRepository.findByStudentIdAndSocialType(data.userId, data.socialType)
    let responseSocialMedia: SocialMedia

    if (socialMedia) {
      responseSocialMedia = await this.socialMediaRepository.update({
        ...newData,
        id: socialMedia.id
      })
    } else {
      responseSocialMedia = await this.socialMediaRepository.register({ ...newData, userId: data.userId })
    }

    if (!responseSocialMedia) {
      return left(new GenericError({ message: "Cannot update social media" }))
    }

    return right({
      message: 'Social media updated successfully',
      handle: { socialMedia: responseSocialMedia },
    })
  }

  async delete(deleteSocialMediaDto: DeleteSocialMediaDto): SocialMediaResponse {
    const payload = deleteSocialMediaSchema.safeParse(deleteSocialMediaDto)

    if (!payload.success) return left(new ValidationError(payload.error))

    await this.socialMediaRepository.delete(payload.data.id)

    return right({
      message: "Social media deleted successfully",
      handle: {}
    })
  }
}