import { z } from 'zod'
import bcrypt from 'bcryptjs'

import { DomainError } from '@/errors/domain-error'
import { IServiceResponse } from '@/types/service-response'
import { AsyncEither, left, right } from '@/utils/either'
import { UserRepository } from '@/repositories/user.repository'
import { CurricumRepository } from '@/repositories/curriculum.repository'
import { createUserSchema, getUserSchema } from '@/types'
import { ValidationError } from '@/errors/validation-error'
import { GenericError } from '@/errors/generic-error'
import { UnauthorizedError } from '@/errors/unhauthorized-error'

type CreateUserDto = z.infer<typeof createUserSchema>
type GetUserDto = z.infer<typeof getUserSchema>
type UserResponse = AsyncEither<DomainError, IServiceResponse>

export class UserCases {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly curriculumRepository: CurricumRepository
  ) { }

  async create(createUserDto: CreateUserDto): UserResponse {
    const payload = createUserSchema.safeParse(createUserDto)

    if (!payload.success) return left(new ValidationError(payload.error))

    const { data } = payload

    const encryptedPassword = await bcrypt.hash(data.password, 10)
    data.password = encryptedPassword

    const response = await this.userRepository.create({
      name: data.name,
      lastname: data.lastname,
      email: data.email,
      password: encryptedPassword
    })

    await this.curriculumRepository.create(response.id)

    if (!response) {
      return left(new GenericError({ message: "Cannot create user" }))
    }

    return right({ message: "User created successfully", handle: { user: response } })
  }

  async getUser(getUserDto: GetUserDto): UserResponse {
    const payload = getUserSchema.safeParse(getUserDto)

    if (!payload.success) return left(new ValidationError(payload.error))

    const user = await this.userRepository.findUserById(payload.data.userId)

    if (!user) {
      return left(new UnauthorizedError())
    }

    return right({ message: "User retrivied successfully", handle: { user } })
  }
}