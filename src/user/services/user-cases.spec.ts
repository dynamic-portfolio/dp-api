import { describe, expect, it, vi } from 'vitest'

import bcrypt from 'bcryptjs'

import { asMock } from '@/utils/asMock'
import { UserCases } from './user-cases'
import { UserRepository } from '@/repositories/user.repository'
import { CurricumRepository } from '@/repositories/curriculum.repository'

const user = {
  name: 'user_name',
  lastname: 'user_lastname',
  email: 'user@email.com',
  password: 'password_hashed'
}

const completeUser = {
  ...user, id: 'user_id', socialMedia: []
}

const userRepositoryMock = {
  create: vi.fn().mockResolvedValue(user),
  findUserById: vi
    .fn()
    .mockResolvedValue(completeUser)
}

const curriculumRepositoryMock = {
  create: vi.fn().mockResolvedValue({
    userId: completeUser.id
  })
}

const userUseCase =
  new UserCases(
    asMock<UserRepository>(userRepositoryMock),
    asMock<CurricumRepository>(curriculumRepositoryMock)
  )

describe('User use cases', () => {
  it('should create user sucessfuly', async () => {
    // @ts-expect-error Lib with issue on mock
    vi.spyOn(bcrypt, 'hash').mockResolvedValue('password_hashed')

    const result = await userUseCase.create(user)

    expect(result.isRight()).toBe(true)
    expect(result.value).toEqual({
      message: "User created successfully",
      handle: { user: user }
    })
    expect(userRepositoryMock.create).toHaveBeenCalledWith(
      user
    )
    expect(curriculumRepositoryMock.create).toHaveBeenCalled()
  })

  it('should get user sucessfuly', async () => {
    const result = await userUseCase.getUser({
      userId: completeUser.id
    })

    expect(result.isRight()).toBe(true)
    expect(result.value).toEqual({
      message: "User retrivied successfully",
      handle: { user: completeUser }
    })
    expect(userRepositoryMock.findUserById).toHaveBeenCalledWith(completeUser.id)
  })
})
