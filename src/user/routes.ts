import { FastifyInstance } from 'fastify'

import { UserController } from './controller'
import { SocialMediaCases, UserCases } from './services'

import { SocialMediaController } from './controller/social-media'
import { request } from 'http'


export const userRoutes = (
    fastify: FastifyInstance,
    userCases: UserCases,
    socialMediaCases: SocialMediaCases
) => {
    const userController = new UserController(userCases)
    const socialMediaController = new SocialMediaController(socialMediaCases)

    fastify.post('/user/create', async (request, reply) => {
        await userController.createUserHandler(request, reply)
    })

    fastify.get('/user', async (request, reply) => {
        await userController.getUserHandler(request, reply)
    })

    fastify.patch('/user/update-social-media', async (request, reply) => {
        await socialMediaController.updateSocialMediaHandler(request, reply)
    })

    fastify.delete('/user/delete-social-media', async (request, reply) => {
        await socialMediaController.deleteAcademicExperienceHandler(request, reply)
    })

}
