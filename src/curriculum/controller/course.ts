import { FastifyReply, FastifyRequest } from "fastify"

import { CourseCases } from "@/curriculum/services"
import { RegisterCourse, UpdateCourse } from "@/types"

type DeleteObject = {
  id: string
}

export class CourseController {
  private readonly courseCases: CourseCases

  constructor(courseCases: CourseCases) {
    this.courseCases = courseCases
  }

  async registerCourseHandler(request: FastifyRequest, reply: FastifyReply) {
    const courseData: RegisterCourse = request.body as RegisterCourse

    return reply.code(200).send((await this.courseCases.register(courseData)).value)
  }

  async updateCourseHandler(request: FastifyRequest, reply: FastifyReply) {
    const courseData: UpdateCourse = request.body as UpdateCourse

    return reply.code(200).send((await this.courseCases.update(courseData)).value)
  }

  async deleteCourseHandler(request: FastifyRequest, reply: FastifyReply) {
    const courseData: DeleteObject = request.body as DeleteObject

    return reply.code(200).send((await this.courseCases.delete(courseData)).value)
  }
}