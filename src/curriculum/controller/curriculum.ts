import { FastifyReply, FastifyRequest } from "fastify"

import { CurriculumCases } from "@/curriculum/services"


export class CurriculumController {
  private readonly curriculumCases: CurriculumCases

  constructor(curriculumCases: CurriculumCases) {
    this.curriculumCases = curriculumCases
  }

  async getCurriculumHandler(request: FastifyRequest, reply: FastifyReply) {
    const { userId } = request.query as { userId: string }

    return reply.code(200).send(
      (await this.curriculumCases.getCurriculum({ userId } as { userId: string })).value
    )
  }
}
