import { FastifyReply, FastifyRequest } from "fastify"

import { AcademicExperienceCases } from "@/curriculum/services"
import {
  IRegisterAcademicExperience,
  IUpdateAcademicExperience
} from "@/types/academic-experience"

type DeleteObject = {
  id: string
}

export class AcademicExperienceController {
  private readonly academicExperienceCases: AcademicExperienceCases

  constructor(academicExperienceCases: AcademicExperienceCases) {
    this.academicExperienceCases = academicExperienceCases
  }

  async registerAcademicExperienceHandler(request: FastifyRequest, reply: FastifyReply) {
    const academicExperienceData: IRegisterAcademicExperience = request.body as IRegisterAcademicExperience

    return reply.code(200).send((await this.academicExperienceCases.register(academicExperienceData)).value)
  }

  async updateAcademicExperienceHandler(request: FastifyRequest, reply: FastifyReply) {
    const academicExperienceData: IUpdateAcademicExperience = request.body as IUpdateAcademicExperience

    return reply.code(200).send((await this.academicExperienceCases.update(academicExperienceData)).value)
  }

  async deleteAcademicExperienceHandler(request: FastifyRequest, reply: FastifyReply) {
    const deleteAcademicExperienceData: DeleteObject = request.body as DeleteObject

    return reply.code(200).send((await this.academicExperienceCases.delete(deleteAcademicExperienceData)).value)
  }
}
