import { FastifyReply, FastifyRequest } from "fastify"

import { ProfessionalExperienceCases } from "@/curriculum/services"
import {
  IRegisterProfessionalExperience,
  IUpdateProfessionalExperience
} from "@/types/professional-experience"

type DeleteObject = {
  id: string
}


export class ProfessionalExperienceController {
  private readonly professionalExperienceCases: ProfessionalExperienceCases

  constructor(professionalExperienceCases: ProfessionalExperienceCases) {
    this.professionalExperienceCases = professionalExperienceCases
  }

  async registerProfessionalExperienceHandler(request: FastifyRequest, reply: FastifyReply) {
    const academicExperienceData: IRegisterProfessionalExperience = request.body as IRegisterProfessionalExperience

    return reply.code(200).send((await this.professionalExperienceCases.register(academicExperienceData)).value)
  }

  async updateProfessionalExperienceHandler(request: FastifyRequest, reply: FastifyReply) {
    const academicExperienceData: IUpdateProfessionalExperience = request.body as IUpdateProfessionalExperience

    return reply.code(200).send((await this.professionalExperienceCases.update(academicExperienceData)).value)
  }

  async deleteProfessionalExperienceHandler(request: FastifyRequest, reply: FastifyReply) {
    const deleteAcademicExperienceData: DeleteObject = request.body as DeleteObject

    return reply.code(200).send((await this.professionalExperienceCases.delete(deleteAcademicExperienceData)).value)
  }

}