import { FastifyInstance } from 'fastify'

import {
  AcademicExperienceController,
  CourseController,
  CurriculumController,
  ProfessionalExperienceController
} from './controller'
import {
  AcademicExperienceCases, CourseCases, CurriculumCases, ProfessionalExperienceCases
} from './services'

export const curriculumRoutes = (
  fastify: FastifyInstance,
  academicExperienceCases: AcademicExperienceCases,
  professionalExperienceCases: ProfessionalExperienceCases,
  courseCases: CourseCases,
  curriculumCases: CurriculumCases
) => {

  const academicExperienceController = new AcademicExperienceController(
    academicExperienceCases
  )

  const professionalExperienceController = new ProfessionalExperienceController(
    professionalExperienceCases
  )

  const courseController = new CourseController(courseCases)

  const curriculumController = new CurriculumController(curriculumCases)

  fastify.post('/curriculum/register-academic-experience', async (request, reply) => {
    await academicExperienceController.registerAcademicExperienceHandler(request, reply)
  })

  fastify.patch('/curriculum/update-academic-experience', async (request, reply) => {
    await academicExperienceController.updateAcademicExperienceHandler(request, reply)
  })

  fastify.delete('/curriculum/delete-academic-experience', async (request, reply) => {
    await academicExperienceController.deleteAcademicExperienceHandler(request, reply)
  })

  fastify.post('/curriculum/register-professional-experience', async (request, reply) => {
    await professionalExperienceController.registerProfessionalExperienceHandler(request, reply)
  })

  fastify.patch('/curriculum/update-professional-experience', async (request, reply) => {
    await professionalExperienceController.updateProfessionalExperienceHandler(request, reply)
  })

  fastify.delete('/curriculum/delete-professional-experience', async (request, reply) => {
    await professionalExperienceController.deleteProfessionalExperienceHandler(request, reply)
  })

  fastify.post('/curriculum/register-course', async (request, reply) => {
    await courseController.registerCourseHandler(request, reply)
  })

  fastify.patch('/curriculum/update-course', async (request, reply) => {
    await courseController.updateCourseHandler(request, reply)
  })

  fastify.delete('/curriculum/delete-course', async (request, reply) => {
    await courseController.deleteCourseHandler(request, reply)
  })

  fastify.get('/curriculum', async (request, reply) => {
    await curriculumController.getCurriculumHandler(request, reply)
  })
}