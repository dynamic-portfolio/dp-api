import { z } from 'zod'

import { DomainError } from '@/errors/domain-error'
import { deleteCourseSchema, registerCourseSchema, updateCourseSchema } from '@/types'
import { IServiceResponse } from '@/types/service-response'
import { AsyncEither, left, right } from '@/utils/either'
import { CourseRepository } from '@/repositories/course.repository'
import { ValidationError } from '@/errors/validation-error'
import { GenericError } from '@/errors/generic-error'

type RegisterCourseDto = z.infer<typeof registerCourseSchema>
type UpdateCourseDto = z.infer<typeof updateCourseSchema>
type DeleteCourseDto = z.infer<typeof deleteCourseSchema>
type CourseResponse = AsyncEither<DomainError, IServiceResponse>

export class CourseCases {
  constructor(
    private readonly courseRepository: CourseRepository
  ) { }

  async register(
    registerCourseDto: RegisterCourseDto
  ): CourseResponse {
    const payload = registerCourseSchema.safeParse(registerCourseDto)

    if (!payload.success) return left(new ValidationError(payload.error))

    const { data } = payload

    const response = await this.courseRepository.register(data)

    if (!response) {
      return left(new GenericError({ message: "Cannot register professional experience" }))
    }

    return right({
      message: "Course created successfully",
      handle: { course: response }
    })
  }

  async update(
    updateCourseDto: UpdateCourseDto
  ): CourseResponse {
    const payload = updateCourseSchema.safeParse(updateCourseDto)

    if (!payload.success) return left(new ValidationError(payload.error))

    const response = await this.courseRepository.update({ ...payload.data, id: payload.data.id })

    return right({
      message: "Course updated successfully",
      handle: { course: response }
    })
  }

  async delete(
    deleteAcademicExperienceDto: DeleteCourseDto
  ): CourseResponse {
    const payload = deleteCourseSchema.safeParse(deleteAcademicExperienceDto)

    if (!payload.success) return left(new ValidationError(payload.error))

    await this.courseRepository.delete(payload.data.id)

    return right({
      message: "Course deleted successfully",
      handle: {}
    })
  }
}