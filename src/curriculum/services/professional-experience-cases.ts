import { z } from 'zod'

import { DomainError } from '@/errors/domain-error'
import { deleteProfessionalExperienceSchema, registerProfessionalExperienceSchema, updateProfessionalExperienceSchema } from '@/types'
import { IServiceResponse } from '@/types/service-response'
import { AsyncEither, left, right } from '@/utils/either'
import { ProfessionalExperienceRepository } from '@/repositories/professional-experience.repository'
import { ValidationError } from '@/errors/validation-error'
import { GenericError } from '@/errors/generic-error'


type RegisterProfessionalExperienceDto = z.infer<typeof registerProfessionalExperienceSchema>
type UpdateProfessionalExperienceDto = z.infer<typeof updateProfessionalExperienceSchema>
type DeleteProfessionalExperienceDto = z.infer<typeof deleteProfessionalExperienceSchema>
type ProfessionalExperienceResponse = AsyncEither<DomainError, IServiceResponse>

export class ProfessionalExperienceCases {
  constructor(
    private readonly professionalExperienceRepository: ProfessionalExperienceRepository
  ) { }

  async register(
    registerProfessionalExperienceDto: RegisterProfessionalExperienceDto
  ): ProfessionalExperienceResponse {
    const payload = registerProfessionalExperienceSchema.safeParse(registerProfessionalExperienceDto)

    if (!payload.success) return left(new ValidationError(payload.error))

    const { data } = payload

    const response = await this.professionalExperienceRepository.register(data)

    if (!response) {
      return left(new GenericError({ message: "Cannot register professional experience" }))
    }

    return right({
      message: "Professional Experience created successfully",
      handle: { professionalExperience: response }
    })
  }

  async update(
    updateAcademicExperienceDto: UpdateProfessionalExperienceDto
  ): ProfessionalExperienceResponse {
    const payload = updateProfessionalExperienceSchema.safeParse(updateAcademicExperienceDto)

    if (!payload.success) return left(new ValidationError(payload.error))

    const response = await this.professionalExperienceRepository.update({ ...payload.data, id: payload.data.id })

    return right({
      message: "Professional Experience updated successfully",
      handle: { professionalExperience: response }
    })
  }

  async delete(
    deleteAcademicExperienceDto: DeleteProfessionalExperienceDto
  ): ProfessionalExperienceResponse {
    const payload = deleteProfessionalExperienceSchema.safeParse(deleteAcademicExperienceDto)

    if (!payload.success) return left(new ValidationError(payload.error))

    await this.professionalExperienceRepository.delete(payload.data.id)

    return right({
      message: "Professional Experience deleted successfully",
      handle: {}
    })
  }
}