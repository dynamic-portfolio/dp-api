import { describe, expect, it, vi } from 'vitest'

import { asMock } from '@/utils/asMock'
import { ProfessionalExperienceCases } from './professional-experience-cases'
import { ProfessionalExperienceRepository } from '@/repositories/professional-experience.repository'

const registerProfessionalExperience = {
  name: 'Professional Experience',
  startDate: new Date(),
  location: 'Location',
  role: 'Role',
  curriculumId: 'curriculum_id'
}

const upateProfessionalExperience = { ...registerProfessionalExperience, id: 'professional_experience_id' }
const deleteProfessionalExperience = { ...registerProfessionalExperience, id: 'professional_experience_id' }

const professionalExperienceRepositoryMock = {
  register: vi.fn().mockResolvedValue(registerProfessionalExperience),
  update: vi.fn().mockResolvedValue(deleteProfessionalExperience),
  delete: vi.fn().mockResolvedValue('professional_experience_id')
}

const professionalExperienceUseCase =
  new ProfessionalExperienceCases(
    asMock<ProfessionalExperienceRepository>(professionalExperienceRepositoryMock)
  )

describe('Professional Experience use cases', () => {
  it('should register professional experience with all required fields', async () => {
    const result = await professionalExperienceUseCase.register(registerProfessionalExperience)

    expect(result.isRight()).toBe(true)
    expect(result.value).toEqual({
      message: "Professional Experience created successfully",
      handle: { professionalExperience: registerProfessionalExperience }
    })
    expect(professionalExperienceRepositoryMock.register).toHaveBeenCalledWith(registerProfessionalExperience)
  })

  it('should update professional experience successfully', async () => {
    const result = await professionalExperienceUseCase.update(upateProfessionalExperience)

    expect(result.isRight()).toBe(true)
    expect(result.value).toEqual({
      message: "Professional Experience updated successfully",
      handle: { professionalExperience: upateProfessionalExperience }
    })
  })

  it('should delete professional experience successfully', async () => {
    const result = await professionalExperienceUseCase.delete({ id: deleteProfessionalExperience.id })

    expect(result.isRight()).toBe(true)
    expect(result.value).toEqual({
      message: "Professional Experience deleted successfully",
      handle: {}
    })
    expect(professionalExperienceRepositoryMock.delete).toHaveBeenCalledWith(deleteProfessionalExperience.id)
  })
})
