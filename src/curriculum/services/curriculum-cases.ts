import { z } from 'zod'

import { DomainError } from '@/errors/domain-error'
import { IServiceResponse } from '@/types/service-response'
import { AsyncEither, left, right } from '@/utils/either'
import { CurricumRepository } from '@/repositories/curriculum.repository'

import { ValidationError } from '@/errors/validation-error'
import { getCurriculumSchema } from '@/types'
import { UnauthorizedError } from '@/errors/unhauthorized-error'


type getCurriculumDto = z.infer<typeof getCurriculumSchema>
type CurriculumResponse = AsyncEither<DomainError, IServiceResponse>

export class CurriculumCases {
  constructor(
    private readonly curriculumRepository: CurricumRepository
  ) { }

  async getCurriculum(
    getCurriculumByIdDto: getCurriculumDto
  ): CurriculumResponse {
    const payload = getCurriculumSchema.safeParse(getCurriculumByIdDto)

    if (!payload.success) return left(new ValidationError(payload.error))

    const { data } = payload

    const response = await this.curriculumRepository.findCurriculumByUserId(data.userId)

    if (!response) {
      return left(new UnauthorizedError())
    }

    return right({
      message: "Curriculum retrivied successfully",
      handle: { curriculum: response }
    })
  }
}
