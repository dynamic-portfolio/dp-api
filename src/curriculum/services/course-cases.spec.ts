import { describe, expect, it, vi } from 'vitest'

import { asMock } from '@/utils/asMock'
import { CourseCases } from './course-cases'
import { CourseRepository } from '@/repositories/course.repository'
import { Status } from '@/utils/enums'

const registerCourse = {
  name: 'Course',
  time: '100',
  company: 'Company',
  status: Status.DONE,
  curriculumId: 'curriculum_id'
}

const updateCourse = { ...registerCourse, id: 'course_id' }
const deleteCourse = { ...registerCourse, id: 'course_id' }

const courseRepositoryMock = {
  register: vi.fn().mockResolvedValue(registerCourse),
  update: vi.fn().mockResolvedValue(updateCourse),
  delete: vi.fn().mockResolvedValue('course_id')
}

const courseUseCase =
  new CourseCases(
    asMock<CourseRepository>(courseRepositoryMock)
  )

describe('Course use cases', () => {
  it('should register course with all required fields', async () => {
    const result = await courseUseCase.register(registerCourse)

    expect(result.isRight()).toBe(true)
    expect(result.value).toEqual({
      message: "Course created successfully",
      handle: { course: registerCourse }
    })
    expect(courseRepositoryMock.register).toHaveBeenCalledWith(registerCourse)
  })

  it('should update course successfully', async () => {
    const result = await courseUseCase.update(updateCourse)

    expect(result.isRight()).toBe(true)
    expect(result.value).toEqual({
      message: "Course updated successfully",
      handle: { course: updateCourse }
    })
  })

  it('should delete course successfully', async () => {
    const result = await courseUseCase.delete({ id: deleteCourse.id })

    expect(result.isRight()).toBe(true)
    expect(result.value).toEqual({
      message: "Course deleted successfully",
      handle: {}
    })
    expect(courseRepositoryMock.delete).toHaveBeenCalledWith(deleteCourse.id)
  })
})
