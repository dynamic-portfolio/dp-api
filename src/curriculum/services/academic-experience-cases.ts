import { z } from 'zod'

import { DomainError } from '@/errors/domain-error'
import { IServiceResponse } from '@/types/service-response'
import { AsyncEither, left, right } from '@/utils/either'
import { AcademicExperienceRepository } from '@/repositories/academic-experience.repository'
import {
  deleteAcademicExperienceSchema,
  registerAcademicExperienceSchema,
  updateAcademicExperienceSchema
} from '@/types'
import { ValidationError } from '@/errors/validation-error'
import { GenericError } from '@/errors/generic-error'


type RegisterAcademicExperienceDto = z.infer<typeof registerAcademicExperienceSchema>
type UpdateAcademicExperienceDto = z.infer<typeof updateAcademicExperienceSchema>
type DeleteAcademicExperienceDto = z.infer<typeof deleteAcademicExperienceSchema>
type AcademicExperienceResponse = AsyncEither<DomainError, IServiceResponse>

export class AcademicExperienceCases {
  constructor(
    private readonly academicExperienceRepository: AcademicExperienceRepository
  ) { }

  async register(
    registerAcademicExperienceDto: RegisterAcademicExperienceDto
  ): AcademicExperienceResponse {
    const payload = registerAcademicExperienceSchema.safeParse(registerAcademicExperienceDto)

    if (!payload.success) return left(new ValidationError(payload.error))

    const { data } = payload

    const response = await this.academicExperienceRepository.register(data)

    if (!response) {
      return left(new GenericError({ message: "Cannot register academic experience" }))
    }

    return right({
      message: "Academic Experience created successfully",
      handle: { academicExperience: response }
    })
  }

  async update(
    updateAcademicExperienceDto: UpdateAcademicExperienceDto
  ): AcademicExperienceResponse {
    const payload = updateAcademicExperienceSchema.safeParse(updateAcademicExperienceDto)

    if (!payload.success) return left(new ValidationError(payload.error))

    const response = await this.academicExperienceRepository.update({ ...payload.data, id: payload.data.id })

    return right({
      message: "Academic Experience updated successfully",
      handle: { academicExperience: response }
    })
  }

  async delete(
    deleteAcademicExperienceDto: DeleteAcademicExperienceDto
  ): AcademicExperienceResponse {
    const payload = deleteAcademicExperienceSchema.safeParse(deleteAcademicExperienceDto)

    if (!payload.success) return left(new ValidationError(payload.error))

    await this.academicExperienceRepository.delete(payload.data.id)

    return right({
      message: "Academic Experience deleted successfully",
      handle: {}
    })
  }
}
