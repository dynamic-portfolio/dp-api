import { describe, expect, it, vi } from 'vitest'

import { asMock } from '@/utils/asMock'
import { CurriculumCases } from './curriculum-cases'
import { CurricumRepository } from '@/repositories/curriculum.repository'

const user = {
  id: 'user_id',
  name: 'user_name',
  lastname: 'user_lastname',
  email: 'user@email.com',
  password: 'password_hashed',
  socialMedia: []
}

const curriculum = {
  id: 'curriculum_id',
  userId: 'user_id',
  user: user,
  academicExperiences: [],
  professionalExperiences: [],
  courses: [],
}

const curriculumRepositoryMock = {
  findCurriculumByUserId: vi
    .fn()
    .mockResolvedValue(curriculum)
}

const curriulumUseCase =
  new CurriculumCases(
    asMock<CurricumRepository>(curriculumRepositoryMock)
  )

describe('Curriculum use cases', () => {
  it('should retrieve curriculum sucessfuly', async () => {
    const result = await curriulumUseCase.getCurriculum({
      userId: 'user_id'
    })

    expect(result.isRight()).toBe(true)
    expect(result.value).toEqual({
      message: "Curriculum retrivied successfully",
      handle: { curriculum: curriculum }
    })
    expect(curriculumRepositoryMock.findCurriculumByUserId).toHaveBeenCalledWith(
      curriculum.userId
    )
  })
})
