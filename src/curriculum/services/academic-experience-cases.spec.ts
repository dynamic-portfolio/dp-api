import { describe, expect, it, vi } from 'vitest'

import { asMock } from '@/utils/asMock'
import { AcademicExperienceCases } from './academic-experience-cases'
import { AcademicExperienceRepository } from '@/repositories/academic-experience.repository'

const registerAcademicExperience = {
  name: 'Academic Experience',
  startDate: new Date(),
  location: 'Location',
  position: 'Position',
  curriculumId: 'curriculum_id'
}

const upateAcademicExperience = { ...registerAcademicExperience, id: 'academic_experience_id' }
const deleteAcademicExperience = { ...registerAcademicExperience, id: 'academic_experience_id' }

const academicExperienceRepositoryMock = {
  register: vi.fn().mockResolvedValue(registerAcademicExperience),
  update: vi.fn().mockResolvedValue(upateAcademicExperience),
  delete: vi.fn().mockResolvedValue('academic_experience_id')
}

const academicExperienceUseCase =
  new AcademicExperienceCases(
    asMock<AcademicExperienceRepository>(academicExperienceRepositoryMock)
  )

describe('Academic Experience use cases', () => {
  it('should register academic experience with all required fields', async () => {
    const result = await academicExperienceUseCase.register(registerAcademicExperience)

    expect(result.isRight()).toBe(true)
    expect(result.value).toEqual({
      message: "Academic Experience created successfully",
      handle: { academicExperience: registerAcademicExperience }
    })
    expect(academicExperienceRepositoryMock.register).toHaveBeenCalledWith(registerAcademicExperience)
  })

  it('should update academic experience successfully', async () => {
    const result = await academicExperienceUseCase.update(upateAcademicExperience)

    expect(result.isRight()).toBe(true)
    expect(result.value).toEqual({
      message: "Academic Experience updated successfully",
      handle: { academicExperience: upateAcademicExperience }
    })
  })

  it('should delete academic experience successfully', async () => {
    const result = await academicExperienceUseCase.delete({ id: deleteAcademicExperience.id })

    expect(result.isRight()).toBe(true)
    expect(result.value).toEqual({
      message: "Academic Experience deleted successfully",
      handle: {}
    })
    expect(academicExperienceRepositoryMock.delete).toHaveBeenCalledWith(deleteAcademicExperience.id)
  })
})
