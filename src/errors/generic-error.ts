import { DomainError, ErrorCodes } from "./domain-error"


export class GenericError implements DomainError {
  error = 'Generic Error'
  code = ErrorCodes.GENERIC_ERROR
  handle: unknown

  constructor(handle: unknown) {
    this.handle = handle
  }
}