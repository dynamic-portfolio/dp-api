export enum Status {
  IN_PROGRESS = 'IN_PROGRESS',
  DONE = 'DONE'
}

export enum SocialType {
  EMAIL = 'EMAIL',
  GIT_LAB = 'GIT_LAB',
  GIT_HUB = 'GIT_HUB',
  LINKEDIN = 'LINKEDIN'
}