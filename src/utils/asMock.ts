export const asMock = <T>(dependency: unknown) => {
  return dependency as T
}
