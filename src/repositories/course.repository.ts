import { Course } from "@/types"
import { SetOptional, SetRequired } from "type-fest"

export abstract class CourseRepository {
  abstract register(course: SetOptional<Course, 'id'>): Promise<Course>

  abstract update(course: SetRequired<Partial<Course>, 'id'>): Promise<Course>

  abstract delete(id: string): Promise<void>
}