import { AsyncMaybe } from "@/types"
import { SocialMedia } from "@/types"
import { SocialType } from "@/utils/enums"
import { SetOptional, SetRequired } from "type-fest"

export abstract class SocialMediaRepository {
  abstract findByStudentIdAndSocialType(userId: string, socialType: SocialType): AsyncMaybe<SocialMedia>

  abstract register(socialMedia: SetOptional<SocialMedia, 'id'>): Promise<SocialMedia>

  abstract update(socialMedia: SetRequired<Partial<SocialMedia>, 'id'>): Promise<SocialMedia>

  abstract delete(id: string): Promise<void>
}