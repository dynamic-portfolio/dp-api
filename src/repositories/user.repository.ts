import { AsyncMaybe, SocialMedia, User } from "@/types";

export abstract class UserRepository {
    abstract create(user: Omit<User, 'id' | 'curriculum'>): Promise<User>

    abstract findUserById(userId: string): AsyncMaybe<User & { socialMedia: SocialMedia[] }>
}
