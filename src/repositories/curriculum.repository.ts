import { AsyncMaybe } from "@/types"
import { Curriculum } from "@/types"
import { SetOptional, SetRequired } from "type-fest"

export abstract class CurricumRepository {
  abstract create(userId: string): Promise<void>

  abstract findCurriculumByUserId(userId: string): AsyncMaybe<Curriculum>
}