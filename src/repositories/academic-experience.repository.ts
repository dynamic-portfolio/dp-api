import { AcademicExperience } from "@/types/academic-experience"
import { SetOptional, SetRequired } from "type-fest";

export abstract class AcademicExperienceRepository {
  abstract register(academicExperience: SetOptional<AcademicExperience, 'id'>): Promise<AcademicExperience>

  abstract update(
    academicExperience: SetRequired<Partial<AcademicExperience>, 'id'>
  ): Promise<AcademicExperience>

  abstract delete(id: string): Promise<void>
}
