import { ProfessionalExperience } from "@/types"
import { SetOptional, SetRequired } from "type-fest"

export abstract class ProfessionalExperienceRepository {
  abstract register(professionalExperience: SetOptional<ProfessionalExperience, 'id'>): Promise<ProfessionalExperience>

  abstract update(
    professionalExperience: SetRequired<Partial<ProfessionalExperience>, 'id'>
  ): Promise<ProfessionalExperience>

  abstract delete(id: string): Promise<void>
}
