import fastify from 'fastify'
import cors from '@fastify/cors'
import dotenv from 'dotenv'

import routes from '@/routes'

dotenv.config()

const server = fastify()


server.register(cors, {
  origin: 'http://localhost:5173',
  methods: ['GET', 'POST', 'PUT', 'DELETE'],
  allowedHeaders: ['Content-Type', 'Authorization']
})

routes(server)

server.listen({ port: 3030 }, (err, address) => {
  if (err) {
    console.error(err)
    process.exit(1)
  }
  console.log(`Server listening at ${address}`)
})
