export type IServiceResponse<T = unknown> = {
  message: string

} & (T extends Object ? { handle: T } : unknown)