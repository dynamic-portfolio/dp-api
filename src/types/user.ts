import { Curriculum } from "./curriculum"
import { SocialMedia } from "./social-media"

export type User = {
  id: string
  email: string
  name: string
  lastname: string
  password: string
  socialMedia?: SocialMedia[]

  curriculum?: Curriculum
}