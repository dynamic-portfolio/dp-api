export type AcademicExperience = {
  id: string
  name: string
  startDate: Date
  endDate?: Date | null
  description?: string | null
  location: string
  position: string

  curriculumId: string
}

export type IRegisterAcademicExperience = {
  name: string,
  startDate: Date
  endDate?: Date
  description?: string
  location: string
  position: string
  curriculumId: string
}

export type IUpdateAcademicExperience = {
  id: string
  name?: string
  startDate?: Date
  endDate?: Date
  description?: string
  location?: string
  position?: string
}