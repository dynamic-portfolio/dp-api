import { Status } from "@/utils/enums"

export type Course = {
  id: string
  name: string
  time: string
  description?: string | null
  company: string
  status: string

  curriculumId: string
}

export type RegisterCourse = {
  name: string,
  time: string,
  description?: string,
  company: string
  status: Status

  curriculumId: string
}

export type UpdateCourse = {
  id: string,
  name?: string
  time?: string
  description?: string
  company?: string
  status?: Status
}