import { SocialType } from "@/utils/enums"

export type SocialMedia = {
  id: string
  reference: string
  socialType: SocialType | string
  userId: string
}

export type EnsureSocialType = {
  socialType: SocialType;
}
