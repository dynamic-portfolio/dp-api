export type ProfessionalExperience = {
  id: string
  name: string
  startDate: Date
  endDate?: Date | null
  description?: string | null
  location: string
  role: string

  curriculumId: string
}

export type IRegisterProfessionalExperience = {
  name: string,
  startDate: Date
  endDate?: Date
  description?: string
  location: string
  role: string
  curriculumId: string
}

export type IUpdateProfessionalExperience = {
  id: string
  name?: string
  startDate?: Date
  endDate?: Date
  description?: string
  location?: string
  role?: string
}