import z from 'zod'
import { SocialType, Status } from '@/utils/enums'

export const registerAcademicExperienceSchema = z.object({
  name: z.string().min(3),
  startDate: z.date(),
  endDate: z.date().optional(),
  description: z.string().optional(),
  location: z.string(),
  position: z.string(),
  curriculumId: z.string()
})

export const updateAcademicExperienceSchema = z.object({
  id: z.string(),
  name: z.string().min(3).optional(),
  startDate: z.date().optional(),
  endDate: z.date().optional(),
  description: z.string().optional(),
  location: z.string().optional(),
  position: z.string().optional()
}).strip()

export const deleteAcademicExperienceSchema = z.object({
  id: z.coerce.string(),
})

export const registerProfessionalExperienceSchema = z.object({
  name: z.string().min(3),
  startDate: z.date(),
  endDate: z.date().optional(),
  description: z.string().optional(),
  location: z.string(),
  role: z.string(),
  curriculumId: z.string()
})

export const updateProfessionalExperienceSchema = z.object({
  id: z.string(),
  name: z.string().min(3).optional(),
  startDate: z.date().optional(),
  endDate: z.date().optional(),
  description: z.string().optional(),
  location: z.string().optional(),
  role: z.string().optional()
}).strip()

export const deleteProfessionalExperienceSchema = z.object({
  id: z.coerce.string(),
})

export const registerCourseSchema = z.object({
  name: z.string().min(3),
  time: z.string(),
  description: z.string().optional(),
  company: z.string(),
  status: z.nativeEnum(Status),
  curriculumId: z.string()
})

export const updateCourseSchema = z.object({
  id: z.string(),
  name: z.string().min(3).optional(),
  time: z.string().optional(),
  description: z.string().optional(),
  company: z.string().optional(),
  status: z.nativeEnum(Status).optional()
}).strip()

export const deleteCourseSchema = z.object({
  id: z.coerce.string(),
})

export const updateSocialMediaSchema = z.object({
  reference: z.string().min(3),
  socialType: z.nativeEnum(SocialType),
  userId: z.string()
})

export const deleteSocialMediaSchema = z.object({
  id: z.coerce.string(),
})

export const createUserSchema = z.object({
  name: z.string().min(4),
  lastname: z.string().min(4),
  email: z.string().email(),
  password: z.string().min(4)
})

export const getUserSchema = z.object({
  userId: z.string()
})

export const getCurriculumSchema = z.object({
  userId: z.string()
})
