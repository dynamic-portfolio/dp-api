import { AcademicExperience } from "./academic-experience"
import { Course } from "./course"
import { ProfessionalExperience } from "./professional-experience"
import { User } from "./user"

export type Curriculum = {
  id: string
  userId: string

  user: User
  academicExperiences: AcademicExperience[]
  professionalExperiences: ProfessionalExperience[]
  courses: Course[]
}
